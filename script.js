"use strict";

function deadlineFn(teamSpeed, backlog, deadline) {
  const totalPoints = backlog.reduce((acc, curr) => acc + curr, 0);
  const workDaysLeft = getWorkDaysLeft(new Date(), deadline);

  const totalCapacity = teamSpeed.reduce((acc, curr) => acc + curr, 0);
  const totalWorkDaysNeeded = Math.ceil(totalPoints / totalCapacity);

  if (totalWorkDaysNeeded <= workDaysLeft) {
    const daysLeft = Math.floor(workDaysLeft / 5) * 7 + workDaysLeft % 5;
    const daysBeforeDeadline = getWorkDaysLeft(new Date(), deadline, true);

    const daysBeforeDeadlineMessage = `за ${daysBeforeDeadline} ${
      daysBeforeDeadline === 1 ? "день" : "дней"
    } до дедлайна`;

    return alert(`Все задачи будут успешно выполнены за ${daysLeft} ${daysLeft === 1 ? "день" : "дней"} до наступления дедлайна.`);
  }

  const hoursNeeded = totalPoints * 8 / teamSpeed.reduce((acc, curr) => acc + curr, 0);
  const hoursAfterDeadline = parseInt(hoursNeeded - workDaysLeft * 8 * teamSpeed.length);

  return alert(`Команде разработчиков придется потратить дополнительно ${hoursAfterDeadline} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
}


// count work-days, WITHOUT weekend 
function getWorkDaysLeft(fromDate, toDate, excludeWeekend = false) {
  const dayInMillis = 24 * 60 * 60 * 1000;
  let count = 0;
  let currentDate = new Date(fromDate);

  while (currentDate <= toDate) {
    const dayOfWeek = currentDate.getDay();

    if (!excludeWeekend || (dayOfWeek !== 6 && dayOfWeek !== 0)) {
      count++;
    }

    currentDate = new Date(currentDate.getTime() + dayInMillis);
  }

  return  count;

}



const teamSpeed = [4, 5, 6];
const backlog = [3, 4, 5];
const deadline = new Date('2023-06-08');

deadlineFn(teamSpeed, backlog, deadline);


const teamSpeed1 = [144, 45, 96];
const backlog1 = [57773, 48, 50];
const deadline1 = new Date('2023-06-08');

deadlineFn(teamSpeed1, backlog1, deadline1);
